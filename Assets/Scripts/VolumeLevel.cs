﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeLevel : MonoBehaviour
{
    public static float bgmVolume = 1f;
    public static float sfxVolume = 1f;
    public static float maxVolume = 1f;

    public Slider bgmSlider, sfxSlider;

    private AudioSource bgm, sfx;

    void Start()
    {
        bgm = GetComponent<AudioSource>();
        sfx = GetComponent<AudioSource>();

        if (PlayerPrefs.GetInt("FIRSTTIMEOPENING", 1) == 1)
        {
            bgmSlider.value = maxVolume;
            sfxSlider.value = maxVolume;
        }
        else
        {
            bgmSlider.value = PlayerPrefs.GetFloat("BGMSlider");
            sfxSlider.value = PlayerPrefs.GetFloat("SFXSlider");
        }
    }

    public void BGMVolumeSet(float vol)
    {
        bgmVolume = vol;
        bgmSlider.value = bgmVolume;
        PlayerPrefs.SetFloat("BGMSlider", bgmSlider.value);
    }

    public void SFXVolumeSet(float vol)
    {
        sfxVolume = vol;
        sfxSlider.value = sfxVolume;
        PlayerPrefs.SetFloat("SFXSlider", sfxSlider.value);
    }
}

