﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Waiting : MonoBehaviour
{
    public GameObject waitingUI;
    public GameObject quizUI;

    public Text waitingTimeText;

    [SerializeField]
    private float waitingTime;

    void Start()
    {
        waitingTime = 3.0f;
    }

    void Update()
    {
        if (waitingTime <= 0)
        {
            waitingTime = 0;
            waitingUI.SetActive(false);
            quizUI.SetActive(true);
        }
        else
            waitingTime -= Time.deltaTime;

        waitingTimeText.text = waitingTime.ToString("0");
    }
}
