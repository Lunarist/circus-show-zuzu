﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    public string key;

    void Update () 
    { 
        UpdateText();
	}

    void UpdateText()
    {
        if(GetComponent<Text>() != null)
            GetComponent<Text>().text = LocalizationManager.instance.GetLocalizedValue (key);
        else if(GetComponent<TextMesh>() != null)
            GetComponent<TextMesh>().text = LocalizationManager.instance.GetLocalizedValue (key);
    }

}
