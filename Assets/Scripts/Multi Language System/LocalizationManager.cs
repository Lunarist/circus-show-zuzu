﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationManager : MonoBehaviour
{
    public static LocalizationManager instance;

    private Dictionary<string, string> localizedText;
    private bool isReady = false;
    private string missingTextString = "Localized text not found";
    public static string currentFile = "English.json";
    string dataAsJson;

    // Use this for initialization
    void Awake () 
    {
        if (instance == null) {
            instance = this;
        } else if (instance != this)
        {
            Destroy (gameObject);
        }

        DontDestroyOnLoad (gameObject);
    }

    void Start()
    {
        if (PlayerPrefs.GetInt("FIRSTTIMEOPENING", 1) == 1)
        {
            LoadLocalizedText("English.json");
        }
        else
        {
            LoadLocalizedText(PlayerPrefs.GetString("Language"));
        }
    }

    public void LoadLocalizedText(string fileName)
    {
        Debug.Log(fileName);
        localizedText = new Dictionary<string, string> ();
        currentFile = fileName;
        string filePath = Path.Combine(Application.streamingAssetsPath + "/", currentFile);

        if (Application.platform == RuntimePlatform.Android)
        {
            WWW reader = new WWW(filePath);
            while(!reader.isDone) {}

            dataAsJson = reader.text;
        }
        else if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            
            if (filePath.Contains ("://") || filePath.Contains (":///"))
            {
                WWW www = new WWW(filePath);

                dataAsJson = www.text;
            }

            // ReadWebGL(fileName);
            StartCoroutine(ReadWebGL(fileName));
        }
        else
        {
            filePath = Path.Combine(Application.streamingAssetsPath, currentFile);
            dataAsJson = File.ReadAllText (filePath);
        }

        LocalizationData loadedData = JsonUtility.FromJson<LocalizationData> (dataAsJson);

        for (int i = 0; i < loadedData.items.Length; i++) 
        {
            localizedText.Add (loadedData.items [i].key, loadedData.items [i].value);    
        }

        isReady = true;
    }

    public string GetLocalizedValue(string key)
    {
        string result = missingTextString;
        if (localizedText.ContainsKey (key)) 
        {
            result = localizedText [key];
        }

        return result;

    }

    public bool GetIsReady()
    {
        return isReady;
    }

    IEnumerator ReadWebGL(string fileName)
    {
        currentFile = fileName;
        string filePath = Path.Combine(Application.streamingAssetsPath + "/", currentFile);

        if (filePath.Contains ("://") || filePath.Contains (":///"))
        {
            WWW www = new WWW(filePath);
            yield return www.text;

            dataAsJson = www.text;
        }
    }

}
