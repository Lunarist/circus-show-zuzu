﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizeSelection : MonoBehaviour
{
    public LocalizationManager localizeManager;
    public string Language;
    public AudioSource buttonEffect;

    void Start() {
        localizeManager = GameObject.FindWithTag("LanguageManager").GetComponent<LocalizationManager>();
        GetComponent<Button>().onClick.AddListener(() => 
            { 
                buttonEffect.volume = VolumeLevel.sfxVolume;
                buttonEffect.Play();
                localizeManager.LoadLocalizedText(Language); 
                PlayerPrefs.SetString("Language", Language);
            });
    }
}
