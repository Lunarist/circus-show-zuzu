﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ensiklopedia : MonoBehaviour
{
    public GameObject[] pageCarnivore;
    public GameObject[] pageHerbivore;
    public GameObject carnivore;
    public GameObject herbivore;

    public static int sPaging;
    private int anotherPage;
    private int currentPage;

    public AudioSource buttonEffect;

    private void Start()
    {
        currentPage = 0;

        if (Menu.animal == "Carnivore")
        {
            carnivore.SetActive(true);
        }
        else
        {
            herbivore.SetActive(true);
        }
    }

    private void Update()
    {
        buttonEffect.volume = VolumeLevel.sfxVolume;
    }

    public void NextButtonCarnivore()
    {
        SFXButton();

        if (currentPage >= pageCarnivore.Length - 1)
            anotherPage = 0;
        else 
            anotherPage++;

        pageCarnivore[currentPage].SetActive(false);
        pageCarnivore[anotherPage].SetActive(true);
        currentPage = anotherPage;
    }

    public void NextButtonHerbivore()
    {
        SFXButton();

        if (currentPage >= pageHerbivore.Length - 1)
            anotherPage = 0;
        else 
            anotherPage++;

        pageHerbivore[currentPage].SetActive(false);
        pageHerbivore[anotherPage].SetActive(true);
        currentPage = anotherPage;
    }

    public void PreviousButtonCarnivore()
    {
        SFXButton();

        if (currentPage <= 0)
            anotherPage = pageCarnivore.Length - 1;
        else 
            anotherPage--;

        pageCarnivore[currentPage].SetActive(false);
        pageCarnivore[anotherPage].SetActive(true);
        currentPage = anotherPage;
    }

    public void PreviousButtonHerbivore()
    {
        SFXButton();

        if (currentPage <= 0)
            anotherPage = pageHerbivore.Length - 1;
        else 
            anotherPage--;

        pageHerbivore[currentPage].SetActive(false);
        pageHerbivore[anotherPage].SetActive(true);
        currentPage = anotherPage;
    }

    public void HomeButton()
    {
        SFXButton();

        SceneManager.LoadScene("Menu");
    }

    public void SFXButton()
    {
        // buttonEffect.volume = VolumeLevel.sfxVolume;
        buttonEffect.Play();
    }
}
