﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz : MonoBehaviour
{
    public GameObject[] resultObject;

    public Animal[] animalQuiz;
    public Image[] imageChoices;

    public AudioSource quizSound;
    public AudioSource buttonEffect;
    public AudioSource correctEffect, wrongEffect, timeOutEffect;

    public Text stopwatchText;

    private int totalQuestion = 0;
    private int totalCorrect = 0;
    private int answerNumber;
    private int choosedNumber;
    private int correctNumber;

    private float stopwatch = 10f;

    private bool alreadyAnswer;

    void Start()
    {
        if (PlayerPrefs.GetInt("TotalQuestion") >= 10)
            EndOfQuiz();
        else 
        {
            totalQuestion = PlayerPrefs.GetInt("TotalQuestion");
            totalQuestion++;
            PlayerPrefs.SetInt("TotalQuestion", totalQuestion);
        }
        Debug.Log(PlayerPrefs.GetInt("TotalQuestion").ToString());

        int randomNumber = Random.Range(0, animalQuiz.Length);
        answerNumber = randomNumber;

        if (PlayerPrefs.GetInt("TotalQuestion") < 6)
            QuizModelA();
        else
            QuizModelB();

        stopwatchText.text = stopwatch.ToString("0");

        RandomizeImage();
    }

    void Update()
    {
        if (stopwatch <= 0)
        {
            stopwatch = 0;
            if (!alreadyAnswer)
                resultObject[2].SetActive(true);
        }
        else
        {
            
            if (alreadyAnswer)
            {
                float tempTime = stopwatch;
                stopwatch = tempTime;
            }
            else 
                stopwatch -= Time.deltaTime;
        }

        stopwatchText.text = stopwatch.ToString("0");
        
        buttonEffect.volume = VolumeLevel.sfxVolume;
        correctEffect.volume = VolumeLevel.sfxVolume;
        wrongEffect.volume = VolumeLevel.sfxVolume;
        timeOutEffect.volume = VolumeLevel.sfxVolume;
    }

    private void QuizModelA()
    {
        quizSound.clip = animalQuiz[answerNumber].animalSound;
        quizSound.Play();
    }

    private void QuizModelB()
    {
        animalQuiz[answerNumber].Translate(LocalizationManager.currentFile);
        quizSound.clip = animalQuiz[answerNumber].spellingSound;
        quizSound.Play();
    }

    private void RandomizeImage()
    {
        int randomAnswer = Random.Range(0, imageChoices.Length);
        imageChoices[randomAnswer].sprite = animalQuiz[answerNumber].image;
        correctNumber = randomAnswer;

        int[] saveNumber = new int[3];
        int x = 0;

        for (int i = 0; i < imageChoices.Length; i++)
        {
            if (i == randomAnswer && i < imageChoices.Length - 1)
                i++;
            else if (i == randomAnswer && i == imageChoices.Length -1)
                break;

            int randomNumber = Random.Range(0, animalQuiz.Length);
            while(randomNumber == answerNumber)
                randomNumber = Random.Range(0, animalQuiz.Length);

            if (x == 0)
            {
                imageChoices[i].sprite = animalQuiz[randomNumber].image;
                saveNumber[x] = randomNumber;
                x++;
            }
            else
            {
                for (int j = 0; j < x; j++)
                {
                    while (randomNumber == saveNumber[j])
                        randomNumber = Random.Range(0, animalQuiz.Length);
                }
                imageChoices[i].sprite = animalQuiz[randomNumber].image;
                saveNumber[x] = randomNumber;
                x++;
            }
        }
    }

    public void GetValueFromImage(int value)
    {
        quizSound.Stop();
        alreadyAnswer = true;
        choosedNumber = value;

        if (choosedNumber == correctNumber)
        {
            totalCorrect = PlayerPrefs.GetInt("TotalCorrect");
            totalCorrect++;
            PlayerPrefs.SetInt("TotalCorrect", totalCorrect);
            resultObject[0].SetActive(true);
        }
        else 
            resultObject[1].SetActive(true);
    }

    public void ContinueButton()
    {
        buttonEffect.Play();

        if (PlayerPrefs.GetInt("TotalQuestion") >= 10)
            EndOfQuiz();
        else
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void StopButton()
    {
        buttonEffect.Play();
        SceneManager.LoadScene("Menu");
    }

    void EndOfQuiz()
    {
        SceneManager.LoadScene("Scoring");
    }
}
