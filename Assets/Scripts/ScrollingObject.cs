﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingObject : MonoBehaviour
{
    private Rigidbody2D r2d;
    
    private float scrollingSpeed = -2f;

    // Start is called before the first frame update
    void Start()
    {
        r2d = GetComponent<Rigidbody2D>();
        r2d.velocity = new Vector2(0, scrollingSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        r2d.velocity = new Vector2(0, scrollingSpeed);
    }
}
