﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Scoring : MonoBehaviour
{
    public Text scoreText;
    public AudioSource buttonEffect;

    void Start()
    {
        scoreText.text = PlayerPrefs.GetInt("TotalCorrect").ToString() + "/" + PlayerPrefs.GetInt("TotalQuestion").ToString();
    }

    void Update()
    {
        buttonEffect.volume = VolumeLevel.sfxVolume;
    }

    public void HomeButton()
    {
        buttonEffect.Play();
        SceneManager.LoadScene("Menu");
    }
}
