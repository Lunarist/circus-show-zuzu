﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimalDisplay : MonoBehaviour
{
    public GameObject animalDisplayUI;
    public GameObject animalListUI;
    public GameObject page1;
    public GameObject page2;
    public GameObject animalSoundButton;

    public new Text name;
    public Text description;

    public Image image;
    public Image background;

    public AudioSource buttonEffect;
    public AudioSource animalSound;
    public AudioSource spellingSound;

    void Start()
    {
        AudioSource animalSound = GetComponent<AudioSource>();
        AudioSource spellingSound = GetComponent<AudioSource>();
    }

    void Update()
    {
        ShowContent();

        buttonEffect.volume = VolumeLevel.sfxVolume;
    }

    public void NextDisplay()
    {
        StopSound();
        buttonEffect.Play();

        if (AnimalList.pageAnimal == AnimalList.sAnimal.Length - 1)
            AnimalList.pageAnimal = 0;
        else 
            AnimalList.pageAnimal++;

        ShowContent();
    }

    public void PreviousDisplay()
    {
        StopSound();
        buttonEffect.Play();

        if (AnimalList.pageAnimal == 0)
            AnimalList.pageAnimal = AnimalList.sAnimal.Length - 1;
        else 
            AnimalList.pageAnimal--;

        ShowContent();
    }

    public void BackButton()
    {
        StopSound();
        buttonEffect.Play();
        GameObject go = GameObject.FindGameObjectWithTag("BGM");
        go.GetComponent<AudioSource>().Play();

        animalDisplayUI.SetActive(false);
        animalListUI.SetActive(true);

        if (AnimalList.pageAnimal <= 7)
        {
            //Ensiklopedia.sPaging = 0;
            page1.SetActive(true);
            page2.SetActive(false);
        }
        else 
        {
            //Ensiklopedia.sPaging = 1;
            page2.SetActive(true);
            page1.SetActive(false);
        }
    }

    public void AnimalSoundButton()
    {
        animalSound.clip = AnimalList.sAnimal[AnimalList.pageAnimal].animalSound;
        animalSound.Play();
    }

    public void SpellingSoundButton()
    {
        spellingSound.clip = AnimalList.sAnimal[AnimalList.pageAnimal].spellingSound;
        spellingSound.Play();
    }

    private void ShowContent()
    {
        name.text = AnimalList.sAnimal[AnimalList.pageAnimal].name;
        description.text = AnimalList.sAnimal[AnimalList.pageAnimal].description;
        image.sprite = AnimalList.sAnimal[AnimalList.pageAnimal].image;
        background.sprite = AnimalList.sAnimal[AnimalList.pageAnimal].backgroundImage;

        if (!AnimalList.sAnimal[AnimalList.pageAnimal].isSoundReady)
            animalSoundButton.SetActive(false);
        else
            animalSoundButton.SetActive(true);
    }

    private void StopSound()
    {
        animalSound.Stop();
        spellingSound.Stop();
    }
}
