﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Animal", menuName = "ScriptableObjects/Animal",order = 1)]
public class Animal : ScriptableObject
{
    public new string name;
    public string[] translateName;
    public string description;
    public string[] translateDesc;

    public Sprite image;
    public Sprite backgroundImage;

    public AudioClip animalSound;
    public AudioClip spellingSound;
    public AudioClip[] translateSpellingSound;

    public bool isSoundReady;

    public void Translate(string language)
    {
        switch(language)
        {
            case "China.json":
                name = translateName[0];
                description = translateDesc[0];
                spellingSound = translateSpellingSound[0];
                break;
            case "English.json":
                name = translateName[1];
                description = translateDesc[1];
                spellingSound = translateSpellingSound[1];
                break;
            case "French.json":
                name = translateName[2];
                description = translateDesc[2];
                spellingSound = translateSpellingSound[2];
                break;
            case "German.json":
                name = translateName[3];
                description = translateDesc[3];
                spellingSound = translateSpellingSound[3];
                break;
            case "Indonesia.json":
                name = translateName[4];
                description = translateDesc[4];
                spellingSound = translateSpellingSound[4];
                break;
            case "Japan.json":
                name = translateName[5];
                description = translateDesc[5];
                spellingSound = translateSpellingSound[5];
                break;
            case "Melayu.json":
                name = translateName[6];
                description = translateDesc[6];
                spellingSound = translateSpellingSound[6];
                break;
            case "Spain.json":
                name = translateName[7];
                description = translateDesc[7];
                spellingSound = translateSpellingSound[7];
                break;
            default:
                name = translateName[1];
                description = translateDesc[1];
                spellingSound = translateSpellingSound[1];
                break;
        }
    }
}
