﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AnimalList : MonoBehaviour
{
    public enum animalList
    {
        Bear = 0, Cat, Crocodile, Dog, Eagle,
        Lion, Penguin, Seal, Shark, Snake,
        Spider, Stork, Tiger, Whale, Wolf
    }

    public GameObject animalListUI;
    public GameObject animalDisplayUI;

    public static Animal[] sAnimal;
    public Animal[] animal;

    public new Text[] name;
    
    public Image[] image;

    public static int pageAnimal;

    public AudioSource buttonEffect;

    private void Start()
    {
        sAnimal = animal;

        for (int i = 0; i < animal.Length; i++)
        {
            animal[i].Translate(LocalizationManager.currentFile);
            name[i].text = animal[i].name;
            image[i].sprite = animal[i].image;
        }
    }

    void Update()
    {
        buttonEffect.volume = VolumeLevel.sfxVolume;
    }

    public void GetEnum(int value)
    {
        buttonEffect.Play();
        GameObject go = GameObject.FindGameObjectWithTag("BGM");
        go.GetComponent<AudioSource>().Stop();

        switch(value)
        {
            case (int)animalList.Bear :
                pageAnimal = (int)animalList.Bear;
                break;
            case (int)animalList.Cat :
                pageAnimal = (int)animalList.Cat;
                break;
            case (int)animalList.Crocodile :
                pageAnimal = (int)animalList.Crocodile;
                break;
            case (int)animalList.Dog :
                pageAnimal = (int)animalList.Dog;
                break;
            case (int)animalList.Eagle :
                pageAnimal = (int)animalList.Eagle;
                break;
            case (int)animalList.Lion :
                pageAnimal = (int)animalList.Lion;
                break;
            case (int)animalList.Penguin :
                pageAnimal = (int)animalList.Penguin;
                break;
            case (int)animalList.Seal :
                pageAnimal = (int)animalList.Seal;
                break;
            case (int)animalList.Shark :
                pageAnimal = (int)animalList.Shark;
                break;
            case (int)animalList.Snake :
                pageAnimal = (int)animalList.Snake;
                break;
            case (int)animalList.Spider :
                pageAnimal = (int)animalList.Spider;
                break;
            case (int)animalList.Stork :
                pageAnimal = (int)animalList.Stork;
                break;
            case (int)animalList.Tiger :
                pageAnimal = (int)animalList.Tiger;
                break;
            case (int)animalList.Whale :
                pageAnimal = (int)animalList.Whale;
                break;
            case (int)animalList.Wolf :
                pageAnimal = (int)animalList.Wolf;
                break;
            default :
                break;
        }

        animalListUI.SetActive(false);
        animalDisplayUI.SetActive(true);
    }

    public void HomeButton()
    {
        buttonEffect.Play();
        SceneManager.LoadScene("Menu");
    }
}
