﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GameObject parentalUI;
    public GameObject startUI;
    public GameObject playUI;
    public GameObject settingsUI;
    public GameObject ensiklopediaUI;
    public GameObject languageUI;

    public AudioSource buttonEffect;

    public static bool isOpened;
    public static string animal;

    private void Start()
    {
        if (PlayerPrefs.GetInt("FIRSTTIMEOPENING", 1) == 1)
        {
            Debug.Log("First Time Opening");
            PlayerPrefs.SetInt("FIRSTTIMEOPENING", 0);
            languageUI.SetActive(true);
        }
        else
        {
            Debug.Log("NOT First Time Opening");
            startUI.SetActive(true);
        }

        PlayerPrefs.DeleteKey("TotalQuestion");
        PlayerPrefs.DeleteKey("TotalCorrect");
    }

    private void Update()
    {
        buttonEffect.volume = VolumeLevel.sfxVolume;
    }

    public void PlayMenu()
    {
        SFXButton();
        startUI.SetActive(!startUI.activeSelf);
        playUI.SetActive(!playUI.activeSelf);
    }

    public void SettingsMenu()
    {
        SFXButton();
        settingsUI.SetActive(!settingsUI.activeSelf);
    }

    public void Exit()
    {
        SFXButton();
        Application.Quit();
    }

    public void EnsiklopediaMenu()
    {
        SFXButton();
        playUI.SetActive(!playUI.activeSelf);
        ensiklopediaUI.SetActive(!ensiklopediaUI.activeSelf);
    }

    public void CarnivoreMenu()
    {
        SFXButton();
        animal = "Carnivore";
        SceneManager.LoadScene("Ensiklopedia");
    }

    public void HerbivoreMenu()
    {
        SFXButton();
        animal = "Herbivore";
        SceneManager.LoadScene("Ensiklopedia");
    }

    public void QuizMenu()
    {
        SFXButton();
        GameObject go = GameObject.FindGameObjectWithTag("BGM");
        Destroy(go);
        SceneManager.LoadScene("Quiz");
    }

    public void BackButton()
    {
        SFXButton();

        if (playUI.activeInHierarchy)
        {
            playUI.SetActive(!playUI.activeSelf);
            startUI.SetActive(!startUI.activeSelf);
        }

        if (ensiklopediaUI.activeInHierarchy)
        {
            ensiklopediaUI.SetActive(!ensiklopediaUI.activeSelf);
            playUI.SetActive(!playUI.activeSelf);
        }
    }

    public void HomeButton()
    {
        SFXButton();

        if (ensiklopediaUI.activeInHierarchy)
        {
            ensiklopediaUI.SetActive(!ensiklopediaUI.activeSelf);
            startUI.SetActive(!startUI.activeSelf);
        }

        if (playUI.activeInHierarchy)
        {
            playUI.SetActive(!playUI.activeSelf);
            startUI.SetActive(!startUI.activeSelf);
        }
    }

    public void LanguageUIDisplay()
    {
        SFXButton();
        languageUI.SetActive(!languageUI.activeSelf);

        if (!startUI.activeInHierarchy)
            startUI.SetActive(true);
    }

    public void ParentalUIDisplay()
    {
        SFXButton();
        parentalUI.SetActive(!parentalUI.activeSelf);
    }

    public void SFXButton()
    {
        buttonEffect.volume = VolumeLevel.sfxVolume;
        buttonEffect.Play();
    }
}
