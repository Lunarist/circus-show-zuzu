﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteBackground : MonoBehaviour
{
    private BoxCollider2D backgroundCollider;

    private float backgroundSize;

    // Start is called before the first frame update
    void Start()
    {
        backgroundCollider = GetComponent<BoxCollider2D>();
        backgroundSize = backgroundCollider.size.y;   
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -backgroundSize)
            RepeatBackground();
    }

    void RepeatBackground()
    {
        Vector2 offset = new Vector2(0, backgroundSize * 2f);
        transform.position = (Vector2)transform.position + offset;
    }
}
